-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 16, 2023 at 11:23 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `jadwal`
--

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE `login` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `Password` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `penjadwalan`
--

CREATE TABLE `penjadwalan` (
  `id` int(12) NOT NULL,
  `Nama` varchar(100) NOT NULL,
  `HariTanggal` date NOT NULL,
  `ket` varchar(55) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `penjadwalan`
--

INSERT INTO `penjadwalan` (`id`, `Nama`, `HariTanggal`, `ket`) VALUES
(7, 'Nur Merlianti', '2023-05-30', ''),
(43, 'Alzainab ', '2023-06-08', 'Belum Melaksanakan'),
(44, 'Falisah Aulia Noni ', '2068-05-10', 'terlaksana'),
(45, 'Arum Nayla ', '2023-06-02', 'Belum Melaksanakan'),
(46, 'saya cantik', '2023-06-07', 'Belum Melaksanakan'),
(47, 'saya cantik', '2023-06-02', 'Belum Melaksanakan'),
(48, 'Ummi Nurliza', '2023-06-13', 'Belum Melaksanakan'),
(49, 'bebi', '2023-06-23', 'terlaksana'),
(50, 'Ummi ', '2023-06-10', 'Belum Melaksanakan'),
(51, 'Arum Nayla ', '2023-06-07', 'Belum Melaksanakan'),
(52, 'saya cantik', '2023-06-23', 'Belum Melaksanakan'),
(53, 'Ummi Nurliza ', '2030-05-22', 'Be'),
(54, 'Arum Nayla ', '2023-06-22', 'Belum Melaksanakan'),
(55, 'Nurbaiti', '2023-06-29', 'Belum Melaksanakan'),
(57, 'Arum Nayla ', '2023-06-23', 'terlaksanakan');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `login`
--
ALTER TABLE `login`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `penjadwalan`
--
ALTER TABLE `penjadwalan`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `login`
--
ALTER TABLE `login`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `penjadwalan`
--
ALTER TABLE `penjadwalan`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
