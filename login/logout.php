<?php
session_start();

// Hapus semua data session
session_destroy();

// Redirect ke halaman login atau halaman lain yang diinginkan setelah logout
header("Location: login2.php");
exit();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>logout</title>
</head>
<body>

</body>
</html>